# LibreNotes

**LibreNotes** &ndash; приложение для хранения заметок с возможностью их синхронизации между устройствами.

## Технологии

Для разработки серверной части используется язык программирования [Python](https://www.python.org/) и фреймворк для веб-разработки [Django](https://www.djangoproject.com/).

[Фронтенд](https://gitlab.com/librenotes/frontend)
