ENTER_DJANGO:=docker-compose exec django

help: ## show make targets
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | \
	awk 'BEGIN {FS = ":.*?## "}; \
		{printf "\033[36m%-25s", $$1} \
		/#__danger/ {printf "\033[31m%s ", "DANGER"} \
		{gsub(/#__danger /, ""); printf "\033[0m%s\n", $$2}'

build: ## build necessary stuff for our project to run (docker images)
	@$(MAKE) clean-py || printf "\n\033[33mWasn't able to execute clean-py, maybe some file permissions issue! But build anyway.\033[0m\n\n"
	docker-compose build

run: ## start containers with docker-compose and attach to logs
	docker-compose up

rund: ## start containers with docker-compose (detached mode)
	docker-compose up -d

stop: ## stop all running containers for this project
	docker-compose stop

start: ## start containers with docker-compose without rebuilding
	docker-compose start

restart: ## just restart containers
	docker-compose stop && docker-compose start

enter: ## enter the Django container (want to play freely with manage.py commands? just `make enter` and have fun)
	$(ENTER_DJANGO) sh

shell: ## enter the Django container and run the shell_plus or shell
	$(ENTER_DJANGO) python manage.py shell_plus || $(ENTER_DJANGO) python manage.py shell

shell-caddy: ## get caddy shell
	docker exec -ti caddy /bin/sh

shell-web: ## get web django shell
	docker exec -ti django /bin/sh

shell-db: ## get postgres shell
	docker exec -ti postgres /bin/sh

logs: ## attach to logs of containers for this project
	docker-compose logs -f

log-caddy: ## get caddy logs
	docker-compose logs caddy

log-web: ## get web logs
	docker-compose logs web

log-db: ## get db logs
	docker-compose logs db

list-packages: ## list installed Python packages with pipdeptree or pip freeze. If pipdeptree is available it list possible dependencies conflicts
	$(ENTER_DJANGO) pipdeptree || $(ENTER_DJANGO) pip freeze

list-outdated: ## list outdated Python packages
	$(ENTER_DJANGO) pip list --format=columns --user --outdated

py-info: ## useful info about your Python Environment (Container)
	@printf "\n\033[33mEnvironment Variables\033[0m\n\n"
	@$(ENTER_DJANGO) printenv
	@printf "\n\033[33mPython Version\033[0m\n\n"
	@$(ENTER_DJANGO) python -V
	@printf "\n\033[33mPython Packages Installed via pipdeptree or pip freeze\033[0m\n\n"
	@$(MAKE) list-packages

clean-py: ## clean python artifacts (cache, tests, build stuff...)
	find ./$(PROJECT_DIR_NAME) -name '*.pyc' -exec rm -f {} + \
	&& find ./$(PROJECT_DIR_NAME) -name '*.pyo' -exec rm -f {} + \
	&& find ./$(PROJECT_DIR_NAME) -name '*~' -exec rm -f {} + \
	&& find ./$(PROJECT_DIR_NAME) -name '__pycache__' -exec rm -fr {} + \
	&& find ./$(PROJECT_DIR_NAME) -name '*.egg-info' -exec rm -fr {} + \
	&& find ./$(PROJECT_DIR_NAME) -name '*.egg' -exec rm -f {} +

remove: ## stop running containers and remove them
	@$(MAKE) stop
	docker-compose rm -f

remove-full: ## #__danger stop and remove running containers, VOLUMES and NETWORKS associated to them
	@echo "\033[33mATTENTION!\033[0m"
	@echo "This command will remove your containers and volumes related to them."
	@read -p "Do you want to proceed? y or n: " answer; if [ "$$answer" != "y" ]; then echo "Operation Cancelled"; exit 1; fi
	docker-compose down --volumes

first-run: ## #__danger This will run migrations and create a INSECURE superuser that you should only use in dev environment
	@echo "\033[33mATTENTION!\033[0m"
	@echo "This command will execute:"
	@echo "- Django Migrations (manage.py migrate all tables will be created)"
	@echo "- Create an \033[31mINSECURE\033[0m superuser that you should only use in dev environment"
	@echo "  - Username: a"
	@echo "  - Password: a\n"

	@read -p "Do you want to proceed? y or n: " answer; if [ "$$answer" != "y" ]; then echo "Operation Cancelled"; exit 1; fi

	$(ENTER_DJANGO) python manage.py migrate
	$(ENTER_DJANGO) python manage.py shell -c "from django.contrib.auth import get_user_model; User = get_user_model(); User.objects.create_superuser('a', 'a@a.co', 'a')"

collectstatic: ## collect static
	docker exec django /bin/sh -c "python manage.py collectstatic --noinput"

postgres-ip: ## get postgres ip
	docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' postgres

postgres-start: ## start only postgres db
	docker-compose build db && docker-compose up -d db

# TODO: test and poetry to pip export targets