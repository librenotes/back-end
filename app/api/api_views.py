from rest_framework import permissions
from rest_framework.generics import CreateAPIView
from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.response import Response
from rest_framework.authtoken.models import Token

from notes import models

from .serializers import UserSerializer


class UserCreateAPIView(CreateAPIView):

    model = get_user_model()
    permission_classes = [permissions.AllowAny]
    serializer_class = UserSerializer

    def post(self, request, *args, **kwargs):
        self.create(request, *args, **kwargs)
        user = models.User.objects.get(username=request.POST.get("username"))
        token = str(Token.objects.get_or_create(user=user)[0])
        return Response({"token": token}, status=status.HTTP_200_OK)
