from rest_framework import serializers

from notes import models


class UserSerializer(serializers.ModelSerializer):

    password = serializers.CharField(write_only=True)

    def create(self, validated_data):

        user = models.User.objects.create(username=validated_data["username"], email=validated_data["email"])
        user.set_password(validated_data["password"])
        user.save()

        return user

    class Meta:
        model = models.User
        fields = ("id", "username", "email", "password")


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Tag
        fields = ("id", "uuid", "name")


class TagsField(serializers.PrimaryKeyRelatedField):
    def get_queryset(self):
        return models.Tag.objects.filter(author=self.context["request"].user)


class NoteSerializer(serializers.ModelSerializer):
    tags = TagsField(many=True, required=False)

    class Meta:
        model = models.Note
        fields = ("id", "uuid", "text", "tags", "created")
